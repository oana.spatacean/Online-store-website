Cerinte si specificatii:

Introducere:
Proiectul "ShoppingWave" reprezintă un magazin online complet funcțional, dezvoltat cu ajutorul tehnologiilor React JS, HTML, CSS și JavaScript. Acest proiect integrează funcționalități esențiale pentru o experiență de cumpărături plăcută, inclusiv autentificare, gestionarea produselor, gestionarea comenzilor și funcționalități de navigare.

Funcționalități principale:

Autentificare:

Sistemul de autentificare este implementat utilizând Auth0.
Doar utilizatorii autentificați pot accesa funcționalitățile complete ale site-ului.
După autentificare, apare un avatar utilizator și un mesaj de salut cu numele utilizatorului.

Baze de Date:

Sunt folosite două baze de date Firebase:
a. Prima bază de date conține comenzile efectuate după checkout.
b. A doua bază de date este utilizată pentru adăugarea, actualizarea și ștergerea produselor disponibile pe site.

Navigare:

Pagina principală (Home) conține un buton "Shop Now" care direcționează utilizatorul către pagina de produse.
Toate paginile includ un footer cu informații suplimentare.

Listarea Produselor:

Pe pagina de produse, inițial sunt afișate toate produsele disponibile.
Utilizatorii pot filtra produsele după categorie de haine și pot utiliza bara de căutare pentru a găsi produse specifice.

Detalii Produs:

La fiecare produs, utilizatorii pot vizualiza detalii complete și pot adăuga produsul în coș.
După adăugare, apare un mesaj pop-up care confirmă adăugarea produsului în coș.

Coș de Cumpărături:

În colțul din dreapta-sus al paginii, există o iconiță de coș de cumpărături.
Utilizatorii pot accesa coșul și pot ajusta cantitatea fiecărui produs selectat.
Acesta afișează suma finală și oferă opțiunea de finalizare a comenzii.

Finalizarea Comenzii:

După finalizarea comenzii, utilizatorii sunt direcționați către un formular în care trebuie să introducă numele, adresa și detaliile cardului de credit.
După completarea formularului și apăsarea butonului "Submit," comanda este trimisă cu succes, iar un mesaj pop-up confirmă acest lucru.
Comenzile sunt stocate în baza de date Firebase.

Nume și Logo:

Numele magazinului online este "ShoppingWave" și este însoțit de o iconiță corespunzătoare.

Administrarea Produselor:

Administrarea produselor este realizată cu ajutorul Firestore, permitând adăugarea, actualizarea și ștergerea produselor pe site.

Redirecționare:

Utilizatorii neautentificați sunt redirecționați către pagina de autentificare și nu pot adăuga produse în coș sau accesa coșul de cumpărături.

Proiectul a fost dezvoltat în Visual Studio Code.
